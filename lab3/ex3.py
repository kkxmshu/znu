# Упражнение 3
# Дан текстовый файл. Сформируйте статистику файла: размер (в байтах), количество
# символов, слов, предложений и абзацев, количество пустых строк и повторяющихся
# пробелов. Сформируйте новый файл, в котором буду удалены лишнее пробелы и пустые
# строки из данного текста.
import os


def delete_empty_lines(arr):
    new = []
    for x in arr:
        if x != '':
            new.append(x)
    return new


def normalize(file, statistic=True):
    content = file.read()
    file.seek(0, os.SEEK_END)
    size = file.tell()
    symbols = len(content)
    spaces = content.count(' ')
    new_lines = content.count('\n')
    words = len(content.split())
    if statistic:
        print('size: {} bytes\nsymbols: {}\nwhite spaces: {}\nnew lines: {}'
              '\nwords: {} \n------\n'.format(size, symbols, spaces, new_lines, words))
    return '\n'.join(delete_empty_lines([' '.join(line.split()) for line in content.splitlines()]))

with open('sample.txt') as f:
    s = normalize(f)

print(s)

with open('result.txt', 'w') as f:
    f.write(s)
