# Упражнение 1
# Напишите генератор паролей. Пароль должен быть случайным, обязательно содержать
# заглавные и прописные символы, цифры и знак подчеркивания (_). Длину пароля определяет
# пользователь.

from random import choice, randint
import string

alphabet = string.ascii_letters + string.digits


def generate_password(length=15):
    arr = [choice(alphabet) for _ in range(length - 1)]
    index = randint(0, length - 1)
    return ''.join(arr[:index] + ['_'] + arr[index:])


print(generate_password(15))
