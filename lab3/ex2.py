# Упражнение 2
# Используя пакеты BeautifulSoup и requests выведите заголовки всех новостей, размещенных
# на сайте http://znu.edu.ua/.

from bs4 import BeautifulSoup
import urllib3

http = urllib3.PoolManager()
r = http.request('GET', 'https://znu.edu.ua/cms/index.php?action=news/block&site_id=27&lang=ukr&rows=6&template'
                        '=template_news_view_block_front.html&orderby=')
html = BeautifulSoup(r.data, 'html.parser')

print('\nParsing all news')
for title in html.findAll('span', {'class': 'link-text'}):
    print(title.text)
