# Упражнение 4
# Напишите функцию, которая для заданного списка удаляет все повторяющиеся записи
# (результат функция возвращает в виде нового списка).
# Используя конструкцию if __name__ == "__main__": напишите систему тестов для
# разработанных функций. Необходимо показать, что функция корректно работает, для списков
# с произвольными типами элементов.
from testing import test

def without_duplicates(arr):
    seen = set()
    return [x for x in arr if not (x in seen or seen.add(x))]


if __name__ == '__main__':
    test(without_duplicates, [1, 2, 3, 1, 2, 3], [1, 2, 3])
    test(without_duplicates, ['asd', 'asd', 'asd', 1, 2], ['asd', 1, 2])
    print('\n')

array = [1, 1, 2, 3, 4, 5, 6, 5, 5, 4, 3, 6, 8, 9, 10, 11, 2, 1, 12, 3]
print(*without_duplicates(array))
