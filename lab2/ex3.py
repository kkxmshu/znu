# Упражнение 3
# Напишите программу, которая запрашивает у пользователя целое число и затем определяет,
# является ли оно правильным. Для запроса целого числа реализуйте функцию, которая
# выводит предложение ввода и затем возвращает введенное число. Проверка числа на
# правильность, должна также быть реализована в виде отдельной функции.

# Напишите функцию, которая генерирует последовательность чисел Фибоначчи, не
# превышающих введенного числа.

# Используя конструкцию if __name__ == "__main__": напишите систему тестов для
# разработанных функций.
from testing import test


def fibonacci(n):
    return 0 if n == 0 else (1 if n == 1 else fibonacci(n - 1) + fibonacci(n - 2))


def check(value):
    try:
        return int(value)
    except ValueError:
        return 'x'


if __name__ == '__main__':
    print('--- TESTS ---')
    test(fibonacci, 0,  0)
    test(fibonacci, 1,  1)
    test(fibonacci, 14, 377)
    test(check, 'asdf', 'x')
    test(check, 5, 5)
    print(fibonacci(4))
    print('---  END  ---\n')

while True:
    number = check(input('Enter the number: '))
    if number != 'x':
        break
    else:
        print('No! You\'re doing it wrong!')

print(*[fibonacci(x) for x in range(number)])
