# Упражнение 5
# Напишите программу (используя функции!), которая предлагает пользователю ввести строку
# и затем выводит новую строку, в которой слова выведены в обратном порядке. Например,
# «Привет мир» → «мир Привет». Изменение порядка слов должно быть реализовано в виде
# отдельной функции. Используя конструкцию if __name__ == "__main__": напишите
# систему тестов для разработанных функций.
from testing import test


def reverse(string):
    return ' '.join([x for x in reversed(str(string).split(' '))])


if __name__ == '__main__':
    test(reverse, 'Hello world', 'world Hello')
    test(reverse, 'a b c', 'c b a')
    print()


text = input('Your message: ')
print(reverse(text))
