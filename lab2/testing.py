def test(func, arg, expected):
    value = func(arg)
    r = value == expected
    print('{}({}) = {}, expected {}: {}'.format(func.__name__, arg, value, expected, r))
    return r
