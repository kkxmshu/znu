# Упражнение 5
# Создайте программу, которая заполняет два списка случайными целыми числами от 0 до 99 и
# затем находит объединение и пересечение обоих списков как множетв.

from random import randint


def generate(length):
    return [randint(0, 99) for _ in range(length)]


n = 15
first = generate(n)
second = generate(n)

intersection = [i for i in first if i in second]

print(*first)
print(*second)
print('\nintersection:')
print(*intersection)
