# Упражнение 1
# Создайте программу, которая запрашивает у пользователя его имя и возраст. Выведите на
# экран сообщение, в котором будет указан год, в котором пользователю исполнится 100 лет.
# Добавьте в программу код, который запросит у пользователя целое положительной число и
# затем выведет на экран предбудущее сообщение, указанное пользователем количество раз.
from datetime import date

name = input('What\'s your name?\n')
age = int(input('How old are you?\n'))
message = '{} will be 100 year old in {}'.format(name, date.today().year + (100 - age))

print(message)
times = int(input('How many times to repeat?\n'))

print('\n')
for i in range(0, times):
    print(message)
