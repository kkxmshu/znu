# Упражнение 4
# Создайте программу, которая запрашивает у пользователя целое число и формирует список
# всех делителей введенного числа.

number = int(input('Enter your number: '))
dividers = filter(lambda x: number % x == 0, range(1, number + 1))

print(*dividers)
