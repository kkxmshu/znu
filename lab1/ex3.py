# Упражнение 3
# Сформируйте список значений. Например:
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# Напишете программу, которая выводит на экран все элементы, которые больше 5.
# Сформируйте новый список, в который должны попасть все элементы, меньшие 20 (решите
# эту задачу двумя способами).

array = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
new = []

for i in array:
    if i < 20:
        new.append(i)

    if i > 5:
        print(i, end=' ')

print()
for i in new:
    print(i, end=' ')

print('\n\n----- another way -----')
greater_than_five = filter(lambda x: x > 5, array)
print(*greater_than_five)
less_than_twenty = filter(lambda x: x < 20, array)
print(*less_than_twenty)
