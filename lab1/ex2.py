# Упражнение 2
# Запросите у пользователя целое число. Программа должна вывести на экран сообщение о
# том, я является ли число четным или нечетным. Если число при этом делится на 4, то также
# необходимо вывести соответствующее сообщение. Запросите у пользователя еще одно число:
# если первое делится на второе или наоборот: второе делится на первое необходимо вывести
# соответствующие сообщения.

first = int(input('Enter first number: '))

parity = 'even' if first % 2 == 0 else 'odd'

print('Number {} is {} {}'.format(first, parity, 'and divisible by 4' if first % 4 == 0 else ''))

second = int(input('\nEnter the second one: '))
values = [first, second] if first % second == 0 else [second, first] if second % first == 0 else []
message = '{} divisible by {}'.format(values[0], values[1]) if len(values) > 0 else 'Numbers are not divisible'

print(message)
