# Лабораторная работа No 6
# Написать эхо-сервер. Сервер должен слушать указанный TCP порт, транслировать данные из
# входящего соединения в исходящее.
# В качестве проверки привести HTTP запрос браузера, посланный написанному серверу.

import socket
import time


def send_answer(c, status='200 OK', typ='text/plain; charset=utf-8', data=''):
    data = data.encode('utf-8')
    c.send(b'HTTP/1.1 ' + status.encode('utf-8') + b'\r\n')
    c.send(b'Server: super-echo-server\r\n')
    c.send(b'Connection: close\r\n')
    c.send(b'Content-Type: ' + typ.encode('utf-8') + b'\r\n')
    c.send(b'Content-Length: ' + bytes(len(data)) + b'\r\n')
    c.send(b'\r\n')
    c.send(data)


def parse(c, a):
    data = b''

    while not b'\r\n' in data:
        tmp = c.recv(1024)
        if not tmp:
            break
        else:
            data += tmp

    if not data:
        return

    udata = data.decode('utf-8')

    udata = udata.split('\r\n', 1)[0]
    method, address, protocol = udata.split(' ', 2)

    if method != 'GET' or address != '/time.html':
        send_answer(c, '404 Not Found', data='Не найдено')
        return

    answer = '''<!DOCTYPE html>'''
    answer += '''<html><head><title>Время</title></head><body><h1>'''
    answer += time.strftime('%H:%M:%S %d.%m.%Y')
    answer += '''</h1></body></html>'''

    send_answer(c, typ='text/html; charset=utf-8', data=answer)


sock = socket.socket()
sock.bind(('localhost', 14900))
sock.listen(5)
try:
    while True:
        conn, addr = sock.accept()
        print('Connected: ', addr)
        # noinspection PyBroadException
        try:
            parse(conn, addr)
        except:
            send_answer(conn, '500 Internal Server Error', data='Error')
        finally:
            conn.close()
finally:
    sock.close()
